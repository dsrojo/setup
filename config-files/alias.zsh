## System
alias grn='grep -rni'
alias p3='python3'

## Git
alias gbv='git branch -vv'
alias gswm='git switch master'
alias gd1='git diff HEAD~1'

## Terraform
alias tf='terraform'
alias tfi='terraform init'
alias tfv='terraform validate'
alias tfp='terraform plan'
alias tfa='terraform apply'
alias tfr='terraform refresh'
alias tff='terraform fmt'

## Kubernetes
alias k='kubectl'
alias kgp='kubectl get pods --all-namespaces'
alias kgd='kubectl get deployments --all-namespaces'
alias kga='kubectl get services,deployments,pods --all-namespaces'
alias kgnr='kubectl get pods --all-namespaces |grep -v Running'
alias ksdr1='kubectl scale deployment --replicas=1 --all'

## Docker
alias drma='docker rm -f $(docker ps -a -q)'
