#!/usr/bin/env bash

# This script enters every directory inside the current one and
# makes a git fetch, pull on the master branch of each

for d in */; do
    d=${d//\//} # remove the /
    echo -e "\n######## $d ########"
    cd $d
    git fetch --all --prune --jobs=10
    git switch master
    git pull
    git branch --no-color --merged | command grep -vE "^(\+|\*|\s*($(git_main_branch)|development|develop|devel|dev)\s*$)" | command xargs -n 1 git branch -d
    cd ..
done

echo -e "\nAll repos updated"
