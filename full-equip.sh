#!/bin/bash

# This script is a personal tool to set up a new Linux PC from scratch on arch based distros

# Update cache, upgrade packages and install ansible
# sudo pacman -Syu --noconfirm
sudo pacman -S ansible git --noconfirm

# Launch the ansible playbook
sudo ansible-playbook setup.yaml

# Reload fonts-cache
sudo fc-cache -fv

# Install Golang
goVersion=$( echo "$( curl --silent  "https://golang.org/dl/" )" | \
    grep "span class" | \
    grep linux-amd64 | \
    cut -d ">" -f 2 | \
    cut -d "<" -f 1 )
wget https://golang.org/dl/$goVersion -P /tmp
sudo rm -rf /usr/local/go
sudo tar -C /usr/local -xzf /tmp/$goVersion
# This part is commented out cause it is on the ansible playbook
# echo "export PATH=$PATH:/usr/local/go/bin" >> /etc/profile

# Oh my Zsh!
sh -c "$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)" "" --unattended --keep-zshrc

# Powerlevel10k
git clone --depth=1 https://github.com/romkatv/powerlevel10k.git ${ZSH_CUSTOM:-$HOME/.oh-my-zsh/custom}/themes/powerlevel10k

# Some git configs
git config --global pull.ff only
git config --global merge.ff false

# Config files
sudo ansible-playbook configs.yaml
ansible-playbook aur.yaml